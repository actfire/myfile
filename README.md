# myWeb.drawio

## Cloudflare

### 免費 DNS

- 快速：Cloudflare 的全球分佈式 DNS 網路能夠提供快速的解析服務，讓網站能夠更快地載入。
- 安全：Cloudflare 的 DNS 服務支援 DNS over HTTPS（DoH）和 DNS over TLS（DoT），能夠加密 DNS 查詢，保護使用者的隱私和安全。
- 可靠：Cloudflare 的 DNS 網路具有高可用性和容錯能力，能夠有效減少 DNS 解析的故障時間。尤其自行維護 DNS 伺服器容易成為攻擊目標，將DNS 交給 Cloudflare 可降低風險。